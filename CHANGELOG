# 2.0.6 (2025/01/16)

* Fixes
   - (substring "012" 3 3) now work

# 2.0.5 (2025/01/15)

* Fixes
   - syntax expansion of empty lists (bug #44 & #45)
   - match of literal in expanded macro
   - fix matching literals in macros
   - fix quasiquoting in macros

* Changes
   - tr7_read functions now return an error object
     instead of TR7_VOID (bug #46)
   - improved DEBUG_SYNTAX

# 2.0.4 (2025/01/06)

* Fixes
   - allow unquote and unquote-splicing in syntax definitions
   - conformity of define-record-type constructors (bug #43)

* Improvements
   - included extern "C" for c++
   - improved linking with MacOS linker
   - improved if/true/false branching
   - improvement of stack dump
   - foreign functions receive const pointer
   - switched operators
   - improve management of instruction

# 2.0.3 (2024/12/01)

* Fixes
   - back to systematic framing, solves dynamic-wind test

# 2.0.2 (2024/11/15)

* Fixes
   - really fix global safety stack

# 2.0.1 (2024/11/14)

* Fixes
   - revert no variable error
   - bug introduced by CALLSELF (closure frame shall be fresh)
   - fix global safety stack

# 2.0.0 (2024/11/12)

* Fixes
   - detection of misplaced definitions (#18)

* Features
   - new functions 'tr7_play_file' and 'tr7_play_string',
     similar to 'tr7_(load|run)_(file|string)' but with fine
     tuning options and funny name.
   - support of colon prefixed SRFI number (see SRFI 97)
   - new procedure (list-copy* L S E E) in (tr7 extra)
   - new syntax (check-types BOOLEAN) in (tr7 extra)
   - new procedure (tr7-keep-playing) in (tr7 trace)

* Changes
   - forward generation of byte code in bytevector (#20)
   - tr7i stops on first error when loading scripts and report it
     in its exit status
   - for-each procedure family now returns nothing
   - setting record values now returns nothing
   - improved implementation of records
   - split of stack in two parts: data and operand
   - use of callself optimized for tail calls or not
   - use less memory for local variables by spoting scope
   - separation of local variables and closure variables
   - better report of errors and lines

# 1.1.1 (2024/09/30)

* Fixes
   - counting lines when manifest string intraline is read

# 1.1.0 (2024/09/18)

In brief:

- this version no more used wide io of libc, consequently,
  hackers can use printf all around their code
- improved UNICODE support, free characters but string must be UTF8
- refactor of foreign pointers

In details:

* Fixes
   - case-lambda now seen as valid procedure

* Changes
   - new field in middle of tr7_config_t: stack_size_max
   - signature of function for foreign pointer changed (#36)
   - move IO from WIDE to UTF8 (#37): now compatible with printf!
   - add install target in Makefile
   - add predicate `char-unicode?` in library (scheme char)
   - string are enforced to be valid UTF8 with valid UNICODE characters

* Improvements
   - improve formatting of symbols (!13)
   - return UNICODE REPLACEMENT character on character read error
   - write UNICODE REPLACEMENT character when needed
   - implementation of SRFI-69 hash primitives (!18)
     string-hash, string-ci-hash, hash-by-identity, hash
   - report bounds that caused 'out of bound' error (#38)
   - added function tr7_unsafe_list_length
   - added function tr7_unsafe_memv_pair
   - stricter signed/unsigned code
   - added stack limitation check

# 1.0.14 (2024/08/31)

* Fixes
   - fix global-buffer-overflow reported by ASAN (#34)
   - return status of 'tr7_load_file' and 'tr7_load_string'

* Features
   - new functions 'tr7_run_file' and 'tr7_run_string',
     similar to 'tr7_load_file' and 'tr7_load_string'
     but correctly stopping and reporting errors.
   - new functions for getting lastest computed values:
     'tr7_get_values_count', 'tr7_get_values', 'tr7_get_value'
     and 'tr7_get_last_value'

* Improvements
   - clean up of legacy field 'retcode'
   - clean up of legacy LOADF_NESTING
   - provide a default version 'unknown-version'
   - add flag LOADF_GUARDED for REPL

# 1.0.13 (2024/08/22)

* Fixes
   - fix syntax expansion that removed parts without capture expansion
   - fix exponential compilation time (#33)

* Features
   - (features) now export the symbol tr7-VERSION
   - new lib: (tr7 misc) featuring 'tr7-id' and 'tr7-version'

# 1.0.12 (2024/08/15)

* Fixes
   - handle recursive define-syntax correctly (#28)

# 1.0.11 (2024/08/12)

* Fixes
   - reading with comments (#29)

* Improvements
   - detection of bad symbols
   - search in constant strings

# 1.0.10 (2024/02/24)

* Fixes
   - reading from bytevector

# 1.0.9 (2024/02/23)

* Fixes
   - build on MSCV
   - error report location in libraries
   - detection of binary/textual ports
   - definition of path separators
   - zero case in decode_character

* Changed
   - string are also tracked in write-shared
   - internal declaration removed from tr7.h
   - function tr7_set_ports removed
   - detection of binary/textual ports
   - functions tr7_is_error, tr7_is_read_error, tr7_is_file_error,
     tr7_error_message, tr7_error_irritants, tr7_error_stack
     now take only the error as argument
   - functions tr7_set_input_port_file, tr7_set_output_port_file
     and tr7_set_error_port_file take an extra arg: the filename


# 1.0.8 (2023/10/15)

* Fixes
   - emit an error message on failure in define-library (issue#25)
   - compute of lines when compiling
   - error message when reading includes (issue#24)

* Changed
   - more accurate jiffies when possible (posix 1993)
   - more digits for double output
   - improved error report

# 1.0.7 (2023/09/27)

* Fixes
   - don't crash on quoted recursive data (TO BE IMPROVED), restores testing
   - really handle flag COMMON_ROOT_ENV

* Changed
   - foreign C functions collected in library (tr7 foreigns)
   - load-extension handles library of import (can't be builtins)

# 1.0.6 (2023/09/22)

* Added
   - DEBUG_LINES option to remove line tracking

* Fixes
   - fix compiling with option -funsigned-char
   - fix binding values as formals of lambdas
   - fix quoting of syntax parameters

* Changed
   - handling of static record descriptor with macros
   - handling of names of control characters

# 1.0.5 (2023/06/21)

* Added
   - processing of #e and #i
   - library (scheme division) = (srfi 141)

* Fixes
   - detection of overflow in multiplications when builtins not available
   - fix result of floor/ when reminder is nul and denominator is negative
   - fix unquoting after a dot in lists

* Changed
   - internal handling of numbers

# 1.0.4 (2023/05/10)

* Fixes
   - minus 0 is now 0 not 0.0
   - import of (srfi 69) now possible
   - fixe evaluation order in cond-expand

* Added
   - tr7-environment->list get a depth

# 1.0.3 (2023/02/08)

* Fixes
   - fix buggy implementation of 'member' and 'assoc' when the
     predicate is not a standard procedure (not 'eq?', 'eqv?' or 'equal?')
   - fix substraction of reals
   - reintroduce ability to set or refer to undefined global objects
   - fix print of operators

* Added
   - procedure 'tr7-exec-stack' when USE_TR7_DEBUG for getting a copy
     of the current execution stack

# 1.0.2 (2023/02/02)

* Fixes
   - importation of libraries running some initialisation
   - match of litterals in macros
   - exportation of symbols of libraries

* Added
   - foreign pointers
   - builtin boxes
   - more makers for bytevectors
   - macro for validation of INTEGERS
   - more functions for symbols

* Changed
   - naming of foreign functions
   - precise distinction of SIZE versus LENGTH for strings and symbols
   - integer operation are optimized
   - improved handling of proc/syntax/oper/instr internaly
   - internal use of boxes for locations

* In progress
   - handling integer overflow

# 1.0.1 (2022/12/26)

* Fixed
   - order of evaluation

* Added
   - error report with stack

