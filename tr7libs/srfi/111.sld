(define-library (srfi 111)
  (export box box? unbox set-box!)
  (import (scheme box)))
